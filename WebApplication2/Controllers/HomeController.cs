using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication2.DAL;
using WebApplication2.Models;
using System.Text;

namespace WebApplication2.Controllers
{
    public class HomeController : Controller
    {
       /* public ActionResult Index()
        {

            return View();
        }*/

       public ActionResult FizzBuzz()
        {
            Class liczba = new Class();

            return View(liczba);
        }

        [HttpPost]
        public ActionResult FizzBuzz(Class number)
        {

            BazaContext baza = new BazaContext();

            string ip = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            if (string.IsNullOrEmpty(ip))
            {
                ip = Request.ServerVariables["REMOTE_ADDR"];
            }
            number.ip = ip;

            var odp = new StringBuilder();

            if (number.liczba < 1 || number.liczba > 100)
            {
                number.odpowiedz = "Dozwolone są tylko liczby całkowite w przedziale od 1 do 100";
                return View("Odpowiedz", number);
            }

            if (number.liczba % 3 == 0)
            {
                odp.Append("fizz");
                baza.Baza.Add(number);
                baza.SaveChanges();
            }
            if (number.liczba % 5 == 0)
            {
                odp.Append("buzz");
                baza.Baza.Add(number);
                baza.SaveChanges();
            }
            if (number.liczba % 7 == 0)
            {
                odp.Append("wizz");
                baza.Baza.Add(number);
                baza.SaveChanges();
            }
            if (odp.Length == 0)
            { 
                odp.Append(number.liczba);
                baza.Baza.Add(number);
                baza.SaveChanges();
            }
            var ret = new Class { odpowiedz = odp.ToString() };
                return View("Odpowiedz", ret);
            
            
        }
    }
}