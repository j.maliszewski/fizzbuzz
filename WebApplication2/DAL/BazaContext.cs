﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using WebApplication2.Models;

namespace WebApplication2.DAL
{
    public class BazaContext : DbContext
    {
        public BazaContext() : base("Baza")
            {}
        public DbSet<Class> Baza { get; set; }
    }
}