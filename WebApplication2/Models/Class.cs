﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace WebApplication2.Models
{
    public class Class
    {
        [Display(Name = "Podaj liczbe" )]
        [Key]
        public int id { get; set; }
        [Required(ErrorMessage = "Musisz podać liczbę")]
        public int liczba { get; set; }
        public string ip { get; set; }
        public string odpowiedz { get; set; }
    }
}